import { ref, computed } from "vue";
import { defineStore } from "pinia";
import type User from "@/type/User";

export const useUserStore = defineStore("user", () => {
  const dialog = ref(false);
  const editedUser = ref<User>({id: -1, login: "" , name: "" , password: ""});
  let lastId = 3;
  const isTable =ref(true);
  
  const users = ref<User[]>([
    { id: 1, login: "admin", name: "Adminstrator", password: "Abc@1234" },
    { id: 2, login: "user1", name: "User1", password: "Abc@1234" },
  ]);

  const deleteUser = (id: number): void => {
    const index = users.value.findIndex((item) => item.id === id);
    users.value.splice(index, 1);
  };

  const login = (loginName: string , password: string): boolean => {
    const index = users.value.findIndex((item) => item.login === loginName);
    if(index >= 0) {
      const user = users.value[index];
      if(user.password === password) {
        return true;
      }
      return false;
    }
    return false;
  }
  const saveUser = () => {
    if(editedUser.value.id < 0){
      editedUser.value.id = lastId++;
      users.value.push(editedUser.value);
    }else{
      const index = users.value.findIndex((item) => item.id === editedUser.value.id);
      users.value[index] = editedUser.value
    }
    
    dialog.value = false;
    clear();
  }

  const editUser = (user: User) => {
    editedUser.value = {...user}; // JSON.parse(JSON.stringify(user)); 
    dialog.value = true;
  }

  const clear = () =>{
    editedUser.value = {id: -1, login: "" , name: "" , password: ""};
  }
  return { users, deleteUser , dialog , editedUser , clear , saveUser ,editUser , isTable , login};
});
